const Chatroom = require('./Chatroom')

module.exports = function() {
  // mapping of all available chatrooms
  const chatrooms = new Map();

  function addRoom(chatroomName) {
    if (chatroomName !== null) {
      const chkroom = chatrooms.get(chatroomName)
      if (typeof chkroom === 'undefined') {
        chatrooms.set(chatroomName, [chatroomName, Chatroom(chatroomName)]);
      }
    }
  }

  function removeClient(client) {
    chatrooms.forEach(c => c.removeUser(client))
  }

  function getChatroomByName(chatroomName) {
    return chatrooms.get(chatroomName)
  }

  function serializeChatrooms() {
    return Array.from(chatrooms.values()).map(c => c.serialize())
  }

  return {addRoom, removeClient, getChatroomByName, serializeChatrooms}
}
