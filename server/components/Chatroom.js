module.exports = function ({ name }) {
  const members = new Map()
  let chatHistory = []

  function broadcastMessage(message) {
    members.forEach(m => m.emit('messagerecive', message))
  }

  function broadcastCommand(command) {
    members.forEach(m => m.emit('userrecivecommand', command))
  }

  function addEntry(entry) {
    chatHistory = chatHistory.concat(entry)
  }

  function getChatHistory() {
    return chatHistory.slice()
  }

  function addUser(client) {
    members.set(client.id, client)
  }

  function removeUser(client) {
    members.delete(client)

  }

  function serialize() {
    return {
      name,
      numMembers: members.size
    }
  }

  return {
    broadcastMessage,
    broadcastCommand,
    addEntry,
    getChatHistory,
    addUser,
    removeUser,
    serialize
  }
}
