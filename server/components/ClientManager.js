
module.exports = function() {
  const clients = new Map()

  function addClient(client) {
    clients.set(client.id, {client})
  }

  function registerClient(client, user) {
    clients.set(client.id, {client, user})
  }

  function removeClient(client) {
    clients.delete(client.id)
  }

  function getAvailableUsers() {
    const usersTaken = new Set(Array.from(clients.values()).filter(c => c.user).map(c => c.user.name))
    return usersTaken;
  }

  function isUserAvailable(userName) {
    return getAvailableUsers().some(u => u.name === userName)
  }

  function getUserByName(userName) {
    let user = []
     clients.forEach((u,k) => {if(u.user.userName === userName){
       user = u
     }})
    return user
  }

  function removeUserByName(userName){
    let user = getUserByName(userName)
    removeClient(user.client)
  }

  function getUserByClientId(clientId) {
    return (clients.get(clientId) || {}).user.userName
  }

  function getAdminUser() {
    let admins = [];
    clients.forEach((u, k) => {
      if (u.user.role === 1) {
        admins.push(u.client)
      }
    })

    return admins;
  }

  function getAllUsers() {
    let users = [];
    clients.forEach((u, k) => users.push(u.user));
    return users;
  }

  return {
    addClient,
    registerClient,
    getAdminUser,
    removeClient,
    getAvailableUsers,
    isUserAvailable,
    getUserByName,
    getUserByClientId,
    getAllUsers,
    removeUserByName
  }
}
