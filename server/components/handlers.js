const Connector = require('../../db');
const moment = require('moment');
const ROLE_ADMIN = 1;
function makeHandleEvent(client, clientManager, chatroomManager) {
  function ensureExists(getter, rejectionMessage) {
    return new Promise(function(resolve, reject) {
      const res = getter()
      return res
        ? resolve(res)
        : reject(rejectionMessage)
    })
  }

  function ensureUserSelected(clientId) {
    return ensureExists(() => clientManager.getUserByClientId(clientId), 'select user first')
  }

  function ensureValidChatroom(chatroomName) {
    return ensureExists(() => chatroomManager.getChatroomByName(chatroomName), `invalid chatroom name: ${chatroomName}`)
  }

  function ensureValidChatroomAndUserSelected(chatroomName) {
    return Promise.all([
      ensureValidChatroom(chatroomName),
      ensureUserSelected(client.id)
    ]).then(([chatroom, user]) => Promise.resolve({chatroom, user}))
  }

  function handleEvent(chatroomName, createEntry) {
    return ensureValidChatroomAndUserSelected(chatroomName).then(function({chatroom, user}) {
      // append event to chat history
      const entry = {
        user,
        ...createEntry()
      }
      chatroom[1].addEntry(entry)
      // notify other clients in chatroom
      chatroom[1].broadcastMessage({
        chat: chatroomName,
        ...entry
      })
      return chatroom
    })
  }

  return handleEvent
}

module.exports = function(client, clientManager, chatroomManager) {
  const handleEvent = makeHandleEvent(client, clientManager, chatroomManager)

  function handleRegister(userName, userEmail, callback) {
    Connector.addUser(userName, userEmail).then(user => {
      if (typeof clientManager.getUserByName(userName).user !== 'undefined') {
        clientManager.removeUserByName(userName)
      }
      clientManager.registerClient(client, {userName, userEmail, role: user.user.role});
      Connector.updateLastVisitByName(userName)
      callback(user);
    });
  }

  function sendUserList() {
    let users = clientManager.getAllUsers();
    let admins = clientManager.getAdminUser();
    admins.forEach((u, k) => {
      u.emit('userlist', users)
    });
  }

  function handleJoin(chatroomName, user, callback) {
    let chatName = '';
    if (user.role === ROLE_ADMIN) {
      chatName = chatroomName;
      chatroomManager.addRoom(user.userName);
    } else {
      chatName = user.userName;
      chatroomManager.addRoom(chatroomName);
    }

    const createEntry = () => ({event: `joined ${chatroomName}`})

    handleEvent(chatroomName, createEntry).then(function(chatroom) {
      // add member to chatroom
      chatroom[1].addUser(client);
      // send chat history to client
      callback(null, chatroom[1].getChatHistory());
    }).catch(callback);
  }

  function handleLeave(chatroomName, callback) {
    const createEntry = () => ({event: `left ${chatroomName}`})

    handleEvent(chatroomName, createEntry).then(function(chatroom) {
      // remove member from chatroom
      chatroom[1].removeUser(client.id);
      callback(null);
    }).catch(callback);
  }

  function handleMessage({chatroomName, message, user} = {}, callback) {
    if (user === chatroomName) {
      Connector.addMessage(user, 'admin', message);
    } else {
      Connector.addMessage(user, chatroomName, message);
    }
    Connector.updateLastActionByName(user)
    const createEntry = () => ({message});
    handleEvent(chatroomName, createEntry).then((chatroom) => callback(null)).catch(callback);
  }

  function handleCommand({chatroomName, cmd} = {}, callback) {
    Connector.addCommand(chatroomName, cmd);
    const room = chatroomManager.getChatroomByName(chatroomName);
    room[1].broadcastCommand(cmd);
    callback(null);
  }

  function handleAction({userName, action} = {}) {
    Connector.addAction(userName, action);
    Connector.updateLastActionByName(userName);
  }

  function handleGetActionsByName(userName, callback) {
    Connector.getAllUserActions(userName).then(actions => callback(actions));
  }

  function handleGetAllActions(callback) {
    Connector.getAllActions().then(actions => callback(actions));
  }

  function handleGetAllUserMessages(userName, callback) {
    Connector.GetAllUserMessages(userName).then(msgs => callback(msgs))
  }

  function getAllUsers(callback) {
    Connector.getAllUsers().then(usr => {
      let users = [];
      usr.forEach((v, i) => {
        users.push({
          user: v.user.userName,
          created: moment(v.register).format('dddd Do of MMM YYYY h:mm:ss A'),
          lastvisit: moment(v.lastvisit).format('dddd Do of MMM YYYY h:mm:ss A'),
          lastaction: moment(v.lastaction).format('dddd Do of MMM YYYY h:mm:ss A')
        })
      })
      callback(users);
    })
  }

  function getAllUserCommand(userName, callback) {
    Connector.getAllUserCommand(userName).then(cmd => callback(cmd));
  }

  function getUserStatistic(userName, callback) {
    Connector.GetAllUserMessages(userName).then(msgs => {
      const info = [];
      msgs.forEach((v, i) => {
        info.push({
          date: moment(v.date).format('dddd Do of MMM YYYY h:mm:ss A'),
          type: 'Message',
          message: v.message,
          author: v.sender.user.userName
        })
      })
      return info
    }).then((info) => {
      return Connector.getAllUserActions(userName).then(actions => {
        actions.forEach((v, i) => {
          info.push({
            date: moment(v.date).format('dddd Do of MMM YYYY h:mm:ss A'),
            type: 'Action',
            message: v.action,
            author: v.user.user.userName
          })
        })
        return info
      })
    }).then((info) => {
      return Connector.getAllUserCommand(userName).then(cmd => {
        cmd.forEach((v, i) => {
          info.push({
            date: moment(v.date).format('dddd Do of MMM YYYY h:mm:ss A'),
            type: 'Command',
            message: v.command,
            author: 'admin'
          })
        })
        return info
      })
    }).then((info) => {
      info.sort(predicateBy('date'))
      callback(info)
    })
  }

  function predicateBy(prop) {
    return function(a, b) {
      if (a[prop] > b[prop]) {
        return 1;
      } else if (a[prop] < b[prop]) {
        return -1;
      }
      return 0;
    }
  }

  return {
    handleRegister,
    handleJoin,
    handleLeave,
    handleMessage,
    sendUserList,
    handleCommand,
    handleAction,
    handleGetActionsByName,
    handleGetAllActions,
    handleGetAllUserMessages,
    getAllUsers,
    getAllUserCommand,
    getUserStatistic
  }
}
