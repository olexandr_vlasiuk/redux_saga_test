const server = require('http').createServer();
const io = require('socket.io')(server);
const Connector = require('../db');

const makeHandlers = require('./components/handlers');
const ChatroomManager = require('./components/ChatroomManager');
const ClientManager = require('./components/ClientManager');

const clientManager = ClientManager()
const chatroomManager = ChatroomManager();

io.on('connection', function(client) {
  const {
    handleRegister,
    handleJoin,
    handleLeave,
    handleMessage,
    sendUserList,
    handleCommand,
    handleAction,
    handleGetActionsByName,
    getAllUsers,
    getUserStatistic
  } = makeHandlers(client, clientManager, chatroomManager)
  console.log('client connected...', client.id)
  Connector.connect();

  client.on('register', handleRegister);

  client.on('join', handleJoin);

  client.on('leave', handleLeave);

  client.on('message', handleMessage);

  client.on('command', handleCommand);

  client.on('action', handleAction);

  client.on('getactionbyname', handleGetActionsByName);

  client.on('getallusers', getAllUsers);

  client.on('statistic', getUserStatistic);

  setInterval(function(){
    sendUserList();
  }, 1500);

  client.on('disconnect', function() {
    Connector.disconnect();
    console.log('client disconnect...', client.id);
  });

  client.on('error', function(err) {
    console.log('received error from client:', client.id);
    console.log(err);
  });
});

server.listen(3000, function(err) {
  if (err)
    throw err;
  console.log('listening on port 3000');
})
