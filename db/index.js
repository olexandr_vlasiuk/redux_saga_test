const mongoose = require('mongoose');
const Configuration = require('../config/Configuration');
const self = this;
const host = Configuration.db.host;
const database = Configuration.db.database;
const userTable = Configuration.db.userTable;
const messageTable = Configuration.db.messageTable;
const commandTable = Configuration.db.commandTable;
const actionTable = Configuration.db.actionTable;
const mongoOpt = {
  useMongoClient: true
};
// Schema of tables
const userSchema = {
  _id: mongoose.Schema.Types.ObjectId,
  user: {
    userName: String,
    userEmail: String,
    role: Number
  },
  register: {
    type: Date,
    default: Date.now
  },
  lastvisit: {
    type: Date,
    default: Date.now
  },
  lastaction: {
    type: Date,
    default: Date.now
  }
};
const messageSchema = {
  _id: mongoose.Schema.Types.ObjectId,
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: userTable
  },
  reciever: {
    type: mongoose.Schema.Types.ObjectId,
    ref: userTable
  },
  message: String,
  date: {
    type: Date,
    default: Date.now
  }
}
const commandSchema = {
  _id: mongoose.Schema.Types.ObjectId,
  reciever: {
    type: mongoose.Schema.Types.ObjectId,
    ref: userTable
  },
  command: String,
  date: {
    type: Date,
    default: Date.now
  }
}
const actionSchema = {
  _id: mongoose.Schema.Types.ObjectId,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: userTable
  },
  action: String,
  date: {
    type: Date,
    default: Date.now
  }
}

const Connector = function() {
  let UserData;
  let MessageData;
  let CommandData;
  let ActionData;

  self.connect = function() {
    const promise = mongoose.connect(`${host}${database}`, mongoOpt);
    promise.then(() => {
      console.log('Successfully connected to mongoDB.');
      UserData = mongoose.model(userTable, userSchema);
      MessageData = mongoose.model(messageTable, messageSchema);
      CommandData = mongoose.model(commandTable, commandSchema);
      ActionData = mongoose.model(actionTable, actionSchema);
      self.addUser('admin', 'admin@admin.com', 1);
    }, err => console.log('Error ', err));

    return promise;
  }

  self.disconnect = function() {
    mongoose.disconnect();
  };

  // User table
  self.addUser = (userName, userEmail, role = 2) => {
    return self.findUserByName(userName).then((userDB) => {
      if (userDB === null) {
        const user = new UserData({
          _id: new mongoose.Types.ObjectId(),
          user: {
            userName: userName,
            userEmail: userEmail,
            role: role
          }
        })
        return user.save(function(err, data) {
          if (err)
            throw err;
          console.log(`${userName} successfully saved.`);
          return data;
        });
      } else {
        return userDB;
      }
    });
  }

  self.updateLastVisitByName = name => {
    UserData.findOne({
      'user.userName': name
    }, function(err, user) {
      if (err)
        throw err;
      user.lastvisit = Date.now()
      user.save()
    });
  }

  self.updateLastActionByName = name => {
    UserData.findOne({
      'user.userName': name
    }, function(err, user) {
      if (err)
        throw err;
      user.lastaction = Date.now()
      user.save()
    });
  }

  self.findUserByName = name => UserData.findOne({'user.userName': name});

  self.getAllUsers = () => UserData.find({});

  // Message table actions
  self.addMessage = (senderName, recieverName, message) => {
    self.findUserByName(senderName).then((userSender) => self.findUserByName(recieverName).then((userReciever) => {
      const msg = new MessageData({_id: new mongoose.Types.ObjectId(), sender: userSender._id, reciever: userReciever._id, message: message})
      msg.save(function(err, data) {
        if (err)
          throw err;
        console.log(`Message successfully saved.`);
        return data;
      })
    }))
  }

  self.GetAllUserMessages = userName => {
    return self.findUserByName(userName)
    .then(user => MessageData
      .find({$or: [{'sender': user._id}, {'reciever': user._id}]})
      .populate('sender', 'user')
      .populate('reciever', 'user'))
  }

  // Command table actions
  self.addCommand = (recieverName, command) => {
    self.findUserByName(recieverName).then((userReciever) => {
      const cmd = new CommandData({_id: new mongoose.Types.ObjectId(), reciever: userReciever._id, command: command})
      cmd.save(function(err, data) {
        if (err)
          throw err;
        console.log(`Command successfully saved.`);
        return data;
      })
    })
  }

  self.getAllUserCommand = userName => {
    return self.findUserByName(userName).then(user =>
      CommandData.find({'reciever': user._id}).populate('reciever', 'user'))
  }

  // Actions table actions
  self.addAction = (name, action) => {
    UserData.findOne({
      'user.userName': name
    }, function(err, user) {
      const newAction = new ActionData({_id: new mongoose.Types.ObjectId(), user: user._id, action: JSON.stringify(action)});
      newAction.save(function(err, data) {
        if (err)
          throw err;
        console.log(`Action successfully saved.`);
        return data;
      })
    });
  }

  self.getAllUserActions = name => {
    return self.findUserByName(name).then(user =>
      ActionData.find({'user': user._id}).populate('user', 'user')
   )}

  self.getAllActions = () => ActionData.find({}).populate('user', 'user');

  return self;
};

module.exports = new Connector();
