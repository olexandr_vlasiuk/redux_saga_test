module.exports = {
  // db options
  db: {
    host: 'mongodb://localhost:27017/',
    database: 'saga-test',
    userTable: 'test',
    messageTable: 'message',
    commandTable: 'command',
    actionTable: 'action'
  }
};
