Install.

Import database from saga-test.

``` bash
npm install
```

Run the socket server.

``` bash
npm run server
```

Run dev server, clients can be requested on localhost:3001.

``` bash
npm run client
```

For test app exist users: admin user (name: admin, email: admin@admin.com) and one user (name: admin1, email: admin1@admin.com). We can register more users if name is unique.
