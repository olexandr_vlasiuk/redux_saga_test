import {combineReducers} from 'redux';
import messageReducer from './task';

const rootReducer = combineReducers({
  messageReducer,
});

export default rootReducer;
