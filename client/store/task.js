import io from 'socket.io-client';
import {eventChannel, delay} from 'redux-saga';
import {
  take,
  call,
  put,
  fork,
  race,
  cancelled
} from 'redux-saga/effects';

const socketServerURL = 'http://localhost:3000';
const ADD_MESSAGE = 'ADD_MESSAGE';
const CLEAR_MESSAGE = 'CLEAR_MESSAGE';
const ADD_USERLIST = 'ADD_USERLIST';
const SEND_MESSAGE = 'SEND_MESSAGE';
let isAdmin = false;
let socket;

const initialState = {
  chatHistory: [],
  userList: []
};

export default(state = initialState, action) => {
  const {chatHistory, userList} = state;
  switch (action.type) {
    case SEND_MESSAGE:
      socket.emit('message', action.payload);
      return state;
    case ADD_MESSAGE:
      const updatedHistory = [
        ...chatHistory,
        action.payload
      ];
      return { userList, chatHistory: updatedHistory};
    case CLEAR_MESSAGE:
      return {userList, chatHistory: []};
    case ADD_USERLIST:
      return {chatHistory, userList: action.userList};
    default:
      return state;
  }
};


const connect = () => {
  socket = io.connect(socketServerURL)
  return socket;
}

const createSocketChannel = socket => eventChannel((emit) => {
  const handler = (data) => emit(data);
  socket.on('messagerecive', handler);
  return () => socket.off('messagerecive', handler);
})

export const isAdminHandler = hState => {
  isAdmin = hState;
}

export const register = (name, email, callback) =>
  socket.emit('register', name, email, callback)

export const join = (chatroomName, user, cb) =>
  socket.emit('join', chatroomName, user, cb)

export const leave = (chatroomName, cb) =>
  socket.emit('leave', chatroomName, cb)

export const unregisterHandler = () =>
  socket.off('message')

export const getAllUsers = cb =>
  socket.emit('getallusers', cb)

export const getUserStatistic = (userName, cb) =>
  socket.emit('statistic', userName, cb)

export const command = (chatroomName, cmd, cb) =>
  socket.emit('command', {
    chatroomName,
    cmd
  }, cb)

export const actionSend = (userName, action) =>
  socket.emit('action', {userName, action})

const userList = socket => eventChannel(emit => {
  const handler = data => emit(data);
  socket.on('userlist', handler);
  return () => socket.off('userlist', handler);
})

const userCommand = socket => eventChannel(emit => {
  const handler = data => emit(data);
  socket.on('userrecivecommand', handler);
  return () => socket.off('userrecivecommand', handler);
})

const listenUserList = function* () {
  const socketChannel = yield call(userList, socket);
  while (true) {
    const payload = yield take(socketChannel);
    yield put({type: ADD_USERLIST, userList: payload});
  }
};

const listenUserCommand = function* () {
  const socketChannel = yield call(userCommand, socket);
  while (true) {
    const payload = yield take(socketChannel);
    if (!isAdmin)
      eval(payload);
  }
};

const listenMessage = function* () {
  const socketChannel = yield call(createSocketChannel, socket);
  while (true) {
    const payload = yield take(socketChannel);
    yield put({type: ADD_MESSAGE, payload});
  }
}

const listenServerSaga = function* () {
  try {
    yield call(connect);
    yield fork(listenUserList);
    yield fork(listenUserCommand);
    yield fork(listenMessage);

  } catch (error) {
    console.log(error);
  }
};

export const startStopChannel = function* () {
  yield call(listenServerSaga)
};
