import React from 'react';
import Cookies from 'universal-cookie';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Register from './register/register';
import Chat from './chat/chat';
import Commands from './commands/commands';
import StatisticsAction from './stAction/stAction';
import Users from './users/users';
import UserStatistics from './userStatistic/userStatistic';

injectTapEventPlugin()

export default class Root extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      isRegistered: false,
      user: {user:{}}
    }
  }

  componentDidMount() {
    const cookies = new Cookies();
    const data = cookies.get('isRegistered');
    if (typeof data !== 'undefined' && data)
      this.setState({isRegistered: true});
  }

  isRegisteredHandler = (state, user) => this.setState({isRegistered: state, user});

  render() {
    return (
      <MuiThemeProvider>
        <Switch>
          <Route
            exact
            path="/"
            render={props=><Register
              {...props}
              isRegistered={this.state.isRegistered}
              isRegisteredHandler={this.isRegisteredHandler} />}
          />
          <Route
            exact
            path="/chat"
            render={props=><Chat
              {...props}
              user={this.state.user.user}
              chatroomName={this.state.user.user.userName}
              isRegistered={this.state.isRegistered}
              userRole={this.state.user.user.role}
             />}
          />
          <Route
            exact
            path="/commands"
            render={props=><Commands
              {...props}
              user={this.state.user.user}
              isRegistered={this.state.isRegistered}
              userRole={this.state.user.user.role}
             />}
          />
          <Route
            exact
            path="/test-statistics"
            render={props => <StatisticsAction
              {...props}
              user={this.state.user.user}
              isRegistered={this.state.isRegistered} />}
          />
          <Route
            exact
            path="/users"
            render={props => <Users
              {...props}
              user={this.state.user.user}
              isRegistered={this.state.isRegistered}
            />}
          />
          <Route
            exact
            path="/actions-history"
            render={props => <UserStatistics
              {...props}
              user={this.state.user.user}
              isRegistered={this.state.isRegistered}
            />}
          />
        </Switch>
      </MuiThemeProvider>)
  }
}
