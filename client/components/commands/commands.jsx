import React from 'react';
import styled from 'styled-components'
import {FlatButton, Paper, TextField, SelectField, MenuItem} from 'material-ui';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import TabMenu from '../tabMenu/tabMenu';
import './commands.scss';
import {join, leave, unregisterHandler, command, isAdminHandler} from '../../store/task';
import {connect} from 'react-redux';
import {Scrollable, NoDots, OutputText} from './components/styledComponents';
import shortid from 'shortid';

const ROLE_ADMIN = 1;

class Commands extends React.Component {
  constructor(props, context) {
    super(props, context)

    let chatHistoryBuff = [];

    this.state = {
      chatHistory: chatHistoryBuff,
      input: '',
      userList: [],
      selectedValue: this.props.chatroomName,
      prevValue: this.props.chatroomName
    }
  }

  componentDidMount() {
    if (!this.props.isRegistered) {
      this.props.history.push('/');
    }
    if(this.props.user.role === ROLE_ADMIN){
      isAdminHandler(true);
    }
    this.scrollChatToBottom()
  }

  componentDidUpdate() {
    this.scrollChatToBottom()
  }

  componentWillUnmount() {
    leave(this.state.selectedValue)
  }

  onInput = e => this.setState({input: e.target.value})

  onSendCommands = () => {
    if (!this.state.input)
      return
    command(this.state.selectedValue, this.state.input, (err) => {
      if (err)
        return console.error(err)

      this.updateChatHistory({command: `Command ${this.state.chatHistory.length + 1}`, message: this.state.input });
      return this.setState({input: ''})
    })
  }

  onUserListReceived = entry => this.setState({userList: entry})

  updateChatHistory = entry => this.setState({chatHistory: this.state.chatHistory.concat(entry)})

  scrollChatToBottom = () => this.panel.scrollTo(0, this.panel.scrollHeight)

  handleChange = (event, index, value) => {
    this.setState({prevValue: this.state.selectedValue});
    this.setState({
      selectedValue: value,
      chatHistory: []
    }, () => {
      leave(this.state.prevValue)
      if (value != ' ') {
      join(this.state.selectedValue, this.props.user, (err, chatHistory) => {
          if (err)
            console.error(err)
          chatHistoryBuff = chatHistory
        })
      }
    });
  }

  render() {
    return (<div className='chat'>
      <TabMenu {...this.props}/>
      <div className='workplace'>
        <Scrollable innerRef={(panel) => {
            this.panel = panel;
          }}>
          <List>
            {
              this.state.chatHistory.map(({
                command,
                message,
                event
              }, i) => [<NoDots key={'nodot' + i}>
                <ListItem key={shortid.generate()} primaryText={`${command} ${event || ''}`} secondaryText={message && <OutputText>
                    {message}
                  </OutputText>}/>
              </NoDots>
                ])
            }
          </List>
        </Scrollable>
        <div className='input-block'>
         <SelectField
           floatingLabelText="Users"
           value={this.state.selectedValue}
           onChange={this.handleChange}
           className='users-select'>
                <MenuItem value=' ' primaryText=' '/>
                 {
                  this.props.messageReducer.userList.map(({
                    userName,
                    userEmail,
                    role
                  }, i) => {
                    if(role !== 1) {
                    return <MenuItem value={userName} key={'list' + i} primaryText={userName}/>
                  }
                  })
                }
              </SelectField>
          <TextField hintText="Enter a command." floatingLabelText="Enter a command." multiLine={true} rows={4} rowsMax={4} onChange={this.onInput} value={this.state.input} onKeyPress={e => (
              e.key === 'Enter'
              ? this.onSendCommands()
              : null)}/>
        </div>
      </div>
    </div>);
  }
}

export default connect(state => state)(Commands);
