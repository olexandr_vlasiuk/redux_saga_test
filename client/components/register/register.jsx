import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {FlatButton, Paper, TextField} from 'material-ui';
import Cookies from 'universal-cookie';
import './register.scss';
import {connect} from 'react-redux';
import {register} from '../../store/task';

class Register extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      inputName: '',
      inputEmail: '',
      errorName: false,
      errorEmail: false
    }

    this.onInputName = this.onInputName.bind(this);
    this.onInputEmail = this.onInputEmail.bind(this);
  }

  componentDidMount() {
    const cookies = new Cookies();
    let data = cookies.get('userRegData');
    data = (typeof data !== 'undefined')
      ? data
      : {
        userName: '',
        userEmail: ''
      };
    this.setState({inputName: data.userName, inputEmail: data.userEmail});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRegistered)
      this.props.history.push('/chat');
  }

  onInputName = e =>
    this.setState({inputName: e.target.value})

  onInputEmail = e =>
    this.setState({inputEmail: e.target.value})

  onRegister = () => {
    const name = this.state.inputName;
    const email = this.state.inputEmail;
    if (this.checkData(name, email)) {
      const cookies = new Cookies();
      cookies.set('userRegData', {
        userName: name,
        userEmail: email,
        errorName: false,
        errorEmail: false
      }, {path: '/'});
      register(name, email, (data) => this.props.isRegisteredHandler(true, data));
    }
  }

  checkData = (userName, userEmail) => {
    let isValid = true;
    if (userName == '' || userName < 3) {
      this.setState({errorName: true});
      isValid = isValid & false;
    }
    if (userEmail == '' || userEmail.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) == null) {
      this.setState({errorEmail: true});
      isValid = isValid & false;
    }
    return isValid;
  }

  render() {

    return (<Paper zDepth={2} className='register-form'>
      <TextField className='user-name' errorText={this.state.errorName ? 'Name field is reqiured' : ''}
          hintText='Your name' onChange={this.onInputName} value={this.state.inputName}/>
      <TextField className='user-email' errorText={this.state.errorEmail ? 'Email field is reqiured' : ''}
          hintText='Your email' onChange={this.onInputEmail} value={this.state.inputEmail}/>
      <FlatButton className='btn-submit' label='Submit' onClick={this.onRegister}/>
    </Paper>)
  }
}

export default connect(state => state)(Register);
