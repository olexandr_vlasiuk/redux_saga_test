import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import {SelectField, MenuItem} from 'material-ui';
import TabMenu from '../tabMenu/tabMenu';
import { getAllUsers } from '../../store/task';

class Users extends React.Component {
  state = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: true,
    showCheckboxes: false,
    height: '300px',
    list: []
  };

  componentDidMount() {
    if (!this.props.isRegistered) {
      this.props.history.push('/');
    }

    getAllUsers(data=>       
      this.setState({list: data})
    );
  }

  render() {
    return (<div>
      <TabMenu {...this.props} />
      <Table height={this.state.height} fixedHeader={this.state.fixedHeader} fixedFooter={this.state.fixedFooter} selectable={this.state.selectable} multiSelectable={this.state.multiSelectable}>
        <TableHeader displaySelectAll={this.state.showCheckboxes} adjustForCheckbox={this.state.showCheckboxes} enableSelectAll={this.state.enableSelectAll}>
        <TableRow>
            <TableHeaderColumn tooltip="The User">User</TableHeaderColumn>
            <TableHeaderColumn tooltip="The created day">Created date</TableHeaderColumn>
            <TableHeaderColumn tooltip="The last visit">Last Visit</TableHeaderColumn>
            <TableHeaderColumn tooltip="The last action">Last action</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={this.state.showCheckboxes} deselectOnClickaway={this.state.deselectOnClickaway} showRowHover={this.state.showRowHover} stripedRows={this.state.stripedRows}>
          {
            this.state.list.map((row, index) => (<TableRow key={index}>
              <TableRowColumn>{row.user}</TableRowColumn>
              <TableRowColumn>{row.created}</TableRowColumn>
              <TableRowColumn>{row.lastvisit}</TableRowColumn>
              <TableRowColumn>{row.lastaction}</TableRowColumn>
            </TableRow>))
          }
        </TableBody>
        <TableFooter adjustForCheckbox={this.state.showCheckboxes}>
          <TableRow>
            <TableRowColumn>User</TableRowColumn>
            <TableRowColumn>Created date</TableRowColumn>
            <TableRowColumn>Last Visit</TableRowColumn>
            <TableRowColumn>Last action</TableRowColumn>
          </TableRow>
        </TableFooter>
      </Table>
    </div>)
  }
}

export default connect(state => state)(Users);
