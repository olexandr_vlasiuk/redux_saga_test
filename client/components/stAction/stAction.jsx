import React from 'react';
import styled from 'styled-components'
import {FlatButton, Paper, TextField, SelectField, MenuItem} from 'material-ui';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import Divider from 'material-ui/Divider';
import TabMenu from '../tabMenu/tabMenu';
import './stAction.scss';
import { actionSend } from '../../store/task';

export default class Chat extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      value: 'one'
    };
  }

  componentDidMount() {
    if (!this.props.isRegistered) {
      this.props.history.push('/');
    }
  }

  handleChange = (event, index, value) => {
    this.setState({value})
    this.onSendAction({action: 'select', value})
  };

  handleChangeRadio = (event, value) => {
    this.onSendAction({action: 'change', value})
  };

  onSendAction = (action) => {
    actionSend(this.props.user.userName, action)
  }

  render() {
    return (<div className='statistics-action'>
      <TabMenu {...this.props}/>
      <div className='actions'>
        <div className='block'>
          <FlatButton label="Default" onClick={()=> this.onSendAction({action: 'click', value:'Default'})}/>
          <FlatButton label="Primary" primary={true} onClick={()=> this.onSendAction({action: 'click', value:'Primary'})}/>
          <FlatButton label="Secondary" secondary={true} onClick={()=> this.onSendAction({action: 'click', value:'Secondary'})}/>
        </div>
        <div className='block'>
          <SelectField floatingLabelText="Select Field" value={this.state.value} onChange={this.handleChange}>
            <MenuItem value='one' primaryText="One"/>
            <MenuItem value='two' primaryText="Two"/>
            <MenuItem value='three' primaryText="Three"/>
            <MenuItem value='four' primaryText="Four"/>
            <MenuItem value='five' primaryText="Five"/>
          </SelectField>
        </div>
        <div className='block'>
          <RadioButtonGroup name="shipSpeed" defaultSelected="not_light" onChange={this.handleChangeRadio}>
            <RadioButton value="light" label="Simple"/>
            <RadioButton value="not_light" label="Selected by default"/>
          </RadioButtonGroup>
        </div>
        <div className='block'>
          <Checkbox label="Checkbox 1" onCheck={(e,isChecked) => this.onSendAction({action: 'check', value: 'checkbox_1'})} />
          <Checkbox label="Checkbox 2" onCheck={(e,isChecked) => this.onSendAction({action: 'check', value: 'checkbox_2'})} />
        </div>
      </div>
    </div>);
  }
}
