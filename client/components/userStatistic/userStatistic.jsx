import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import {SelectField, MenuItem} from 'material-ui';
import TabMenu from '../tabMenu/tabMenu';
import { getUserStatistic, getAllUsers} from '../../store/task';
import shortid from 'shortid';
import './UserStatistic.scss';

const ROLE_ADMIN = 1;

class UserStatistics extends React.Component {
  state = {
    fixedHeader: true,
    fixedFooter: false,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: true,
    showCheckboxes: false,
    height: '300px',
    list: [],
    selectedValue: '',
    userList: []
  };

  componentDidMount() {
    if (!this.props.isRegistered) {
      this.props.history.push('/');
    }
    if(this.props.user.role !== ROLE_ADMIN){
      this.getForUser(this.props.user.userName);
    } else {
      let uList = [];
      getAllUsers(data => {
        data.forEach(v => {
          uList.push({
            userName: v.user.userName,
            userEmail: v.user.userEmail,
            role: v.user.role
          });
        });
        this.setState({userList: uList});
      })
    }
  }

  getForUser = (userName) => {
    getUserStatistic(userName, data =>
      this.setState({list: data})
    );
  }

  handleChange = (event, index, value) => {
    this.setState({ selectedValue: value });
    this.getForUser(value);
  }

  render() {
    return (<div>
      <TabMenu {...this.props} />
      <Table height={this.state.height} fixedHeader={this.state.fixedHeader} fixedFooter={this.state.fixedFooter} selectable={this.state.selectable} multiSelectable={this.state.multiSelectable}>
        <TableHeader displaySelectAll={this.state.showCheckboxes} adjustForCheckbox={this.state.showCheckboxes} enableSelectAll={this.state.enableSelectAll}>
        <TableRow>
            <TableHeaderColumn tooltip="The Date">Date</TableHeaderColumn>
            <TableHeaderColumn tooltip="The Type">Type</TableHeaderColumn>
            <TableHeaderColumn tooltip="The Message">Message</TableHeaderColumn>
            <TableHeaderColumn tooltip="The Author">Author</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={this.state.showCheckboxes} deselectOnClickaway={this.state.deselectOnClickaway} showRowHover={this.state.showRowHover} stripedRows={this.state.stripedRows}>
          {
            this.state.list.map((row, index) => (<TableRow key={shortid.generate()}>
              <TableRowColumn>{row.date}</TableRowColumn>
              <TableRowColumn>{row.type}</TableRowColumn>
              <TableRowColumn>{row.message}</TableRowColumn>
              <TableRowColumn>{row.author}</TableRowColumn>
            </TableRow>))
          }
        </TableBody>
        <TableFooter adjustForCheckbox={this.state.showCheckboxes}>
          <TableRow>
            <TableRowColumn>Date</TableRowColumn>
            <TableRowColumn>Type</TableRowColumn>
            <TableRowColumn>Message</TableRowColumn>
            <TableRowColumn>Author</TableRowColumn>
          </TableRow>
        </TableFooter>
      </Table>
      {this.props.user.role === ROLE_ADMIN &&
        <SelectField
          floatingLabelText="Users"
           value={this.state.selectedValue}
           onChange={this.handleChange}
           className='user-statistic-select'>
             <MenuItem value=' ' primaryText=' '/>
              {
               this.state.userList.map(({
                 userName,
                 userEmail,
                 role
               }, i) => {
                 if(role !== ROLE_ADMIN) {
                 return <MenuItem value={userName} key={'list' + i} primaryText={userName}/>
               }
               })
             }
           </SelectField>}
    </div>)
  }
}

export default connect(state => state)(UserStatistics);
