import React from 'react';
import {Link} from 'react-router-dom';
import {Tabs, Tab} from 'material-ui';

const ROLE_ADMIN = 1;
export default class TabMenu extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      value: 1
    }
  }

  componentDidMount() {
    switch (this.props.location.pathname) {
      case '/chat':
        this.setState({value: 1})
        break;
      case '/commands':
        this.setState({value: 3})
        break;
      case '/test-statistics':
        this.setState({value:4})
        break;
      case '/users':
        this.setState({value:5})
        break;
      case '/actions-history':
        this.setState({value:6})
        break;
      case '/actions-history':
        this.setState({value:7})
        break;
      default:
        this.setState({value: 1})
        break;
    }
  }

  render() {
    return (<Tabs value={this.state.value}>
      <Tab
        value={1}
        label="Chat"
        containerElement={<Link to="/chat" />}/>
      {this.props.user.role === ROLE_ADMIN && <Tab
        value={3}
        label="Commands"
        containerElement={<Link to="/commands" />}/>}
      {this.props.user.role === ROLE_ADMIN && <Tab
        value={5}
        label="All users"
        containerElement={<Link to="/users" />}/>}
      {this.props.user.role === ROLE_ADMIN && <Tab
        value={6}
        label="Actions history"
        containerElement={<Link to="/actions-history" />}/>}
      {this.props.user.role !== ROLE_ADMIN && <Tab
        value={4}
        label="Test statistics"
        containerElement={<Link to="/test-statistics" />}/>}
      {this.props.user.role !== ROLE_ADMIN && <Tab
        value={7}
        label="Actions history"
        containerElement={<Link to="/actions-history" />}/>}
    </Tabs>)
  }
}
