import React from 'react';
import {FlatButton, Paper, TextField, SelectField, MenuItem} from 'material-ui';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import TabMenu from '../tabMenu/tabMenu';
import './chat.scss';
import {connect} from 'react-redux';
import {join, leave, unregisterHandler, isAdminHandler} from '../../store/task';
import {Scrollable, NoDots, OutputText} from './components/styledComponents';
import shortid from 'shortid';

const ROLE_ADMIN = 1;

class Chat extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      chatHistory: [],
      input: '',
      userList: [],
      selectedValue: this.props.chatroomName === null ? ' ' : this.props.chatroomName,
      prevValue: this.props.chatroomName
    }
  }

  componentDidMount() {
    if (!this.props.isRegistered) {
      this.props.history.push('/');
    }
    if(this.props.user.role === ROLE_ADMIN){
      isAdminHandler(true);
    }
    this.scrollChatToBottom()
    join(this.props.chatroomName, this.props.user, (err, chatHistory) => {
      if (err)
        console.error(err)
        if(typeof chatHistory !== 'undefined')
          this.setState({chatHistory})
    })
  }

  componentDidUpdate() {
    this.scrollChatToBottom()
  }

  componentWillUnmount() {
    unregisterHandler()
    leave(this.state.selectedValue)
  }

  onInput = e => this.setState({input: e.target.value});

  onSendMessage() {
    let payload =     {
      chatroomName: this.state.selectedValue,
      message: this.state.input,
      user: this.props.user.userName
    }
    this.props.dispatch({type: 'SEND_MESSAGE', payload})
    return this.setState({input: ''})
  }

  onMessageReceived = entry => this.updateChatHistory(entry);

  onUserReciveCommand(entry) {
    eval(entry)
  }

  onUserListReceived(entry) {
    this.setState({userList: entry})
  }

  updateChatHistory(entry) {
    this.setState({chatHistory: this.state.chatHistory.concat(entry)})
  }

  scrollChatToBottom() {
    this.panel.scrollTo(0, this.panel.scrollHeight)
  }

  handleChange = (event, index, value) => {
    this.setState({prevValue: this.state.selectedValue});
    this.setState({
      selectedValue: value
    }, () => {
      leave(this.state.prevValue)
      if (value != ' ') {
        join(this.state.selectedValue, this.props.user, (err, chatHistory) => {
          if (err)
            console.error(err)
          if(typeof chatHistory !== 'undefined')
            this.setState({chatHistory})
        })
        this.props.dispatch({type: 'CLEAR_MESSAGE'});
      }
    });
  }

  render() {
    return (<div className='chat'>
      <TabMenu {...this.props}/>
      <div className='workplace'>
        <Scrollable innerRef={(panel) => {
            this.panel = panel;
          }}>
          <List>
            {
              this.props.messageReducer.chatHistory.map(({
                user,
                message,
                event
              }, i) => [<NoDots key={'nodot' + i}>
                <ListItem className={user === this.props.chatroomName
                    ? 'my-message'
                    : ''} key={shortid.generate()} primaryText={`${user} ${event || ''}`} secondaryText={message && <OutputText>
                    {message}
                  </OutputText>}/>
              </NoDots>
                ])
            }
          </List>
        </Scrollable>
        <div className='input-block'>
          {
            this.props.userRole === ROLE_ADMIN && <SelectField
              floatingLabelText="Users"
              value={this.state.selectedValue}
              onChange={this.handleChange}
              className='users-select'>
                <MenuItem value=' ' primaryText=' '/>
                 {
                  this.props.messageReducer.userList.map(({
                    userName,
                    userEmail,
                    role
                  }, i) => {
                    if(role !== 1) {
                    return <MenuItem value={userName} key={'list' + i} primaryText={userName}/>
                  }
                  })
                }
              </SelectField>
          }
          <TextField hintText="Enter a message." floatingLabelText="Enter a message." multiLine={true} rows={4} rowsMax={4} onChange={this.onInput} value={this.state.input} onKeyPress={e => (
              e.key === 'Enter'
              ? this.onSendMessage()
              : null)}/>
        </div>
      </div>
    </div>);
  }
}

export default connect(state => state)(Chat);
